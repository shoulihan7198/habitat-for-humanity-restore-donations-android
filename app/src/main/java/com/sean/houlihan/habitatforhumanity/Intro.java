package com.sean.houlihan.habitatforhumanity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Intro extends AppCompatActivity {

    public static final String MODE_INSERT_NEW = "0";
    public static final String MODE_REQUEST_ID = "-1";
    public static final String MODE_INSERT_EXISTING = "-2";
    public static final String RESULT_SUCCESS = "-3";
    public static final String RESULT_FAILED = "-4";
    public static final String RESULT_NOT_FOUND = "-5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);


        final Button newBtn = (Button) findViewById(R.id.new_btn);
        if (newBtn != null) {
            newBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), NewFormActivity.class);
                    startActivity(intent);
                }
            });
        }
        final Button alreadyBtn = (Button) findViewById(R.id.donated_before_btn);
        if (alreadyBtn != null) {
            alreadyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog alertDialog = new AlertDialog.Builder(v.getContext(), R.style.AlertDialogCustom)
                            .setCancelable(true)
                            .setView(R.layout.email_dialog_layout)
                            .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton(getString(R.string.msg_continue), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(final DialogInterface dialog, int which) {

                                    final String email = ((EditText) ((AlertDialog) dialog).findViewById(R.id.intro_email_input)).getText().toString();
                                    if (!email.isEmpty()) {

                                        dialog.dismiss();
                                        final ProgressDialog progressDialog = new ProgressDialog(((AlertDialog) dialog).getContext(), R.style.AlertDialogCustom);
                                        progressDialog.setIndeterminate(true);
                                        progressDialog.setMessage(getString(R.string.checking));
                                        progressDialog.setCancelable(false);
                                        progressDialog.setCanceledOnTouchOutside(false);
                                        progressDialog.show();

                                        progressDialog.getWindow().getDecorView().setMinimumWidth(Util.convertDpToPixel(400, ((AlertDialog) dialog).getContext()));

                                        AsyncTask<Void, Void, Void> asyncTask = new AsyncTask<Void, Void, Void>() {

                                            private String person_id = "";

                                            @Override
                                            protected Void doInBackground(Void... params) {

                                                Socket client = null;

                                                try {
                                                    client = new Socket(InetAddress.getByName(getString(R.string.server_ip)), 5000);
                                                    client.setSoTimeout(8000);
                                                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(client.getOutputStream());
                                                    BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                                    outputStreamWriter.write(MODE_REQUEST_ID);
                                                    NewFormActivity.writeQueue(outputStreamWriter);
                                                    outputStreamWriter.write(email);
                                                    NewFormActivity.writeQueue(outputStreamWriter);
                                                    person_id = reader.readLine();
                                                    Log.d("lol", person_id);
                                                    reader.close();
                                                    outputStreamWriter.close();
                                                    client.close();

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                return null;

                                            }

                                            @Override
                                            protected void onPostExecute(Void voids) {

                                                progressDialog.dismiss();

                                                if (person_id.isEmpty()) {

                                                    Snackbar.make(newBtn, getString(R.string.error_occurred), Snackbar.LENGTH_LONG).show();

                                                } else if (person_id.equals(RESULT_FAILED)) {

                                                    Snackbar.make(newBtn, getString(R.string.failure), Snackbar.LENGTH_LONG).show();

                                                } else if (person_id.equals(Intro.RESULT_NOT_FOUND)) {

                                                    Snackbar.make(newBtn, getString(R.string.email_not_found), Snackbar.LENGTH_LONG).show();


                                                } else if (!person_id.isEmpty()) {
                                                    Intent intent = new Intent(getApplicationContext(), NewFormActivity.class);
                                                    intent.putExtra("newAccount", false);
                                                    intent.putExtra("personID", person_id);
                                                    intent.putExtra("email", email);
                                                    startActivity(intent);
                                                }

                                            }


                                        };

                                        asyncTask.execute();


                                    } else {
                                        Snackbar.make(newBtn, getString(R.string.enter_valid_email), Snackbar.LENGTH_LONG).show();
                                    }

                                }
                            }).show();

                    final Button okButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    okButton.setEnabled(false);
                    final EditText emailEditText = (EditText) alertDialog.findViewById(R.id.intro_email_input);
                    emailEditText.addTextChangedListener(new TextWatcher() {

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (EmailValidator.validateEmail(s)) okButton.setEnabled(true);
                            else okButton.setEnabled(false);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });



                }
            });
        }

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sharedPreferences.getBoolean("firstLaunch", true) || sharedPreferences.getString("zipCode", "").isEmpty()) {

            AlertDialog dialog = new AlertDialog.Builder(this, R.style.AlertDialogCustom)
                    .setTitle(getString(R.string.first_launch_title))
                    .setView(R.layout.first_launch_dialog_layout)
                    .setCancelable(false)
                    .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("firstLaunch", false);
                            editor.putString("zipCode", ((EditText) ((AlertDialog) dialog).findViewById(R.id.zip_first_launch_edit_text)).getText().toString());
                            editor.apply();

                        }
                    }).show();

            final Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            okButton.setEnabled(false);
            EditText zipFirstLaunch = (EditText) dialog.findViewById(R.id.zip_first_launch_edit_text);
            zipFirstLaunch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 5) okButton.setEnabled(true);
                    else okButton.setEnabled(false);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }

    }
}
