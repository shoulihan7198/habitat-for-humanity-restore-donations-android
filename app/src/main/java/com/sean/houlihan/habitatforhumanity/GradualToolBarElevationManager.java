package com.sean.houlihan.habitatforhumanity;

import android.support.design.widget.AppBarLayout;
import android.util.Log;

/**
 * Created by Sean on 4/20/2016.
 */
public class GradualToolBarElevationManager {

    public static final float TOOLBAR_ELEVATION_DP = 8f;
    private final float GRADUAL_SHADOW_DP_END = 20f;
    private float density;
    private AppBarLayout appbar;


    public GradualToolBarElevationManager(AppBarLayout appbar) {
        ;
        this.appbar = appbar;
        density = appbar.getContext().getResources().getDisplayMetrics().density;
    }

    public void setAppBarLayoutElevationByScroll(int scrollY) {

        float scrollYDP = scrollY / density;

        if (scrollYDP <= GRADUAL_SHADOW_DP_END) {

            float elevation = (scrollYDP / GRADUAL_SHADOW_DP_END) * TOOLBAR_ELEVATION_DP;
            appbar.setElevation(elevation);
            Log.d("SCROLLINGLOG", String.valueOf(elevation));
        } else {
            appbar.setElevation(TOOLBAR_ELEVATION_DP);
        }


    }


}
