package com.sean.houlihan.habitatforhumanity;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by Sean on 3/18/2016.
 */
public class Util {

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return ((int) (dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)));
    }

    public static float convertPixeltoDp(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }





}
