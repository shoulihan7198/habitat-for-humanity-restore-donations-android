package com.sean.houlihan.habitatforhumanity;

import java.util.regex.Pattern;

/**
 * Created by Sean on 3/20/2016.
 */
public class EmailValidator {

    private static final String regexPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern pattern = Pattern.compile(regexPattern);

    public static boolean validateEmail(CharSequence email) {
        return pattern.matcher(email).matches();
    }
}
