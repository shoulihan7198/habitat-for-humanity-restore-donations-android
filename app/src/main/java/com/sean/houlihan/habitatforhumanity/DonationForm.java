package com.sean.houlihan.habitatforhumanity;

import java.util.List;

/**
 * This class is serialized using GSON and sent to the server as a JSON string.
 * The server must have the exact same DonationForm.java file in order to reconstruct
 * the DonationForm object from the JSON. Any changes to fields must be reflected in the
 * server's DonationForm.java as well
 */
public class DonationForm {

    private final String firstName;
    private final String lastName;
    private final String address;
    private final String city;
    private final String state;
    private final String zipCode;
    private final String homePhone;
    private final String cellPhone;
    private final String email;
    private final List<Donation> donations;
    private String personID;
    private byte[] signaturePNG;
    private final String restoreZipCode;

    DonationForm(String firstName, String lastName, String address,
                 String city, String state, String zipCode,
                 String homePhone, String cellPhone, String email,
                 List<Donation> donations, String restoreZipCode) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.homePhone = homePhone;
        this.cellPhone = cellPhone;
        this.email = email;
        this.donations = donations;
        this.restoreZipCode = restoreZipCode;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public void setSignatureByteArray(byte[] array) {
        this.signaturePNG = array;
    }

}

