package com.sean.houlihan.habitatforhumanity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;

import java.text.DecimalFormat;

public class AddDonation extends AppCompatActivity {

    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        final Donation donation = getIntent().getParcelableExtra("donation");
        if (donation != null) {
            setContentView(R.layout.activity_add_donation_edit);
        } else {
            setContentView(R.layout.activity_add_donation);
        }

        ((ImageView) findViewById(R.id.back_arrow)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        index = getIntent().getIntExtra("index", -1);
        final Button addButton = (Button) findViewById(R.id.add_button);
        addButton.setEnabled(false);
        final AppCompatAutoCompleteTextView autoCompleteTextView = (AppCompatAutoCompleteTextView) findViewById(R.id.autoComplete);
        autoCompleteTextView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.autcomplete_donations_list)));
        autoCompleteTextView.setDropDownBackgroundDrawable(new ColorDrawable(getBaseContext().getResources().getColor(R.color.blue)));
        final TextInputLayout itemName = (TextInputLayout) findViewById(R.id.donation_add_name);
        final TextInputLayout itemQuantity = (TextInputLayout) findViewById(R.id.donation_add_number);
        final TextInputLayout itemValue = (TextInputLayout) findViewById(R.id.donation_price_estimate);
        final CheckedTextView itemNew = (CheckedTextView) findViewById(R.id.donation_add_checkbox);
        if (donation != null) {
            itemName.getEditText().setText(donation.getName());
            itemQuantity.getEditText().setText(String.valueOf(donation.getQuantity()));
            itemNew.setChecked(donation.is_new());
            DecimalFormat df = new DecimalFormat("#");
            df.setMaximumFractionDigits(2);
            itemValue.getEditText().setText(df.format(donation.getValue()));
            addButton.setEnabled(true);
            final Button delButton = (Button) findViewById(R.id.delete_button);
            delButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("delete", true);
                    returnIntent.putExtra("index", index);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });
        } else {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
        itemNew.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean checked = itemNew.isChecked();
                if (checked) {
                    itemNew.setChecked(false);
                } else {
                    itemNew.setChecked(true);
                }

            }
        });

        itemName.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (itemName.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemQuantity.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemValue.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else addButton.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        itemQuantity.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (itemQuantity.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemName.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemValue.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else addButton.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        itemValue.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (itemQuantity.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemName.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else if (itemValue.getEditText().getText().toString().isEmpty())
                    addButton.setEnabled(false);
                else addButton.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Donation donation = new Donation(itemName.getEditText().getText().toString(), Integer.parseInt(itemQuantity.getEditText().getText().toString()), itemNew.isChecked(), Double.valueOf(itemValue.getEditText().getText().toString()));
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", donation);
                if (index > -1) {
                    returnIntent.putExtra("index", index);
                }
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {
        cancel();
    }

    private void cancel() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("index", index);
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

}
