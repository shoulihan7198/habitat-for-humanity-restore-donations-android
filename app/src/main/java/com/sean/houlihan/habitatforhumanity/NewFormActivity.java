package com.sean.houlihan.habitatforhumanity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.simplify.ink.InkView;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class NewFormActivity extends AppCompatActivity {

    private Button addDonationBtn;
    private LinearLayout donationLayout;
    private ArrayList<Donation> donations = new ArrayList<>();
    private ImageView submitButton;
    private TextInputLayout firstNameInput;
    private TextInputLayout lastNameInput;
    private TextInputLayout addressInput;
    private TextInputLayout stateInput;
    private TextInputLayout zipInput;
    private TextInputLayout cityInput;
    private TextInputLayout emailInput;
    private TextInputLayout emailInput2;
    private TextInputLayout homePhoneInput;
    private TextInputLayout cellPhoneInput;
    private List<TextInputLayout> inputs;
    private ScrollView scrollView;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String zip;
    private String state;
    private String email;
    private String email2;
    private String homePhone;
    private String cellPhone;
    private boolean newAccount;
    private String personID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        addDonationBtn = (Button) findViewById(R.id.add_button);
        submitButton = (ImageView) findViewById(R.id.submit_button);
        donationLayout = (LinearLayout) findViewById(R.id.donations_layout);
        addDonationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), AddDonation.class);
                startActivityForResult(intent, 1);
            }
        });

        newAccount = getIntent().getBooleanExtra("newAccount", true);
        if (newAccount) {
            firstNameInput = (TextInputLayout) findViewById(R.id.firstNameInput);
            lastNameInput = (TextInputLayout) findViewById(R.id.lastNameInput);
            addressInput = (TextInputLayout) findViewById(R.id.addressInput);
            stateInput = (TextInputLayout) findViewById(R.id.stateInput);
            zipInput = (TextInputLayout) findViewById(R.id.zipInput);
            cityInput = (TextInputLayout) findViewById(R.id.cityInput);
            emailInput = (TextInputLayout) findViewById(R.id.emailInput);
            emailInput2 = (TextInputLayout) findViewById(R.id.emailInput2);
            homePhoneInput = (TextInputLayout) findViewById(R.id.homePhoneInput);
            cellPhoneInput = (TextInputLayout) findViewById(R.id.cellPhoneInput);
            inputs = new ArrayList<>(9);
            inputs.add(firstNameInput);
            inputs.add(lastNameInput);
            inputs.add(addressInput);
            inputs.add(stateInput);
            inputs.add(zipInput);
            inputs.add(cityInput);
            inputs.add(emailInput);
            inputs.add(emailInput2);
            inputs.add(homePhoneInput);
            inputs.add(cellPhoneInput);
        } else {

            LinearLayout.LayoutParams params = ((LinearLayout.LayoutParams) addDonationBtn.getLayoutParams());
            params.setMargins(0, Util.convertDpToPixel(10, getBaseContext()), 0, Util.convertDpToPixel(16, getBaseContext()));
            Snackbar.make(donationLayout, getString(R.string.welcome_back), Snackbar.LENGTH_LONG).show();
            personID = getIntent().getStringExtra("personID");
            email = getIntent().getStringExtra("email");
            (findViewById(R.id.form_content_layout)).setVisibility(View.GONE);
            LinearLayout.LayoutParams layout = (LinearLayout.LayoutParams) donationLayout.getLayoutParams();
            layout.topMargin = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()));

        }

        scrollView = ((ScrollView) findViewById(R.id.scrollView));

        if (Build.VERSION.SDK_INT >= 21) {

            final AppBarLayout appbar = ((AppBarLayout) findViewById(R.id.appbar));
            appbar.setElevation(0);

            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

                private GradualToolBarElevationManager manager = new GradualToolBarElevationManager(appbar);

                @Override
                public void onScrollChanged() {

                    manager.setAppBarLayoutElevationByScroll(scrollView.getScrollY());

                }
            });
        }

        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                boolean valid = false;

                if (newAccount) {

                    firstName = firstNameInput.getEditText().getText().toString();
                    lastName = lastNameInput.getEditText().getText().toString();
                    address = addressInput.getEditText().getText().toString();
                    city = cityInput.getEditText().getText().toString();
                    zip = zipInput.getEditText().getText().toString();
                    state = stateInput.getEditText().getText().toString();
                    email = emailInput.getEditText().getText().toString();
                    email2 = emailInput2.getEditText().getText().toString();
                    homePhone = homePhoneInput.getEditText().getText().toString();
                    cellPhone = cellPhoneInput.getEditText().getText().toString();
                    if (firstName.isEmpty() || lastName.isEmpty() || address.isEmpty() || city.isEmpty() || zip.isEmpty() || state.isEmpty() || !EmailValidator.validateEmail(email) || !EmailValidator.validateEmail(email2) || !email.equals(email2)) {


                        String field_required = getString(R.string.field_required);

                        if (firstName.isEmpty()) firstNameInput.setError(field_required);
                        else firstNameInput.setError(null);
                        if (lastName.isEmpty()) lastNameInput.setError(field_required);
                        else lastNameInput.setError(null);
                        if (address.isEmpty()) addressInput.setError(field_required);
                        else addressInput.setError(null);
                        if (city.isEmpty()) cityInput.setError(field_required);
                        else cityInput.setError(null);
                        if (zip.isEmpty()) zipInput.setError(field_required);
                        else zipInput.setError(null);
                        if (state.isEmpty()) stateInput.setError(field_required);
                        else stateInput.setError(null);
                        if (!email.equals(email2)) {
                            emailInput.setError(getString(R.string.email_dont_match));
                            emailInput2.setError(getString(R.string.email_dont_match));
                        } else {

                            if (!EmailValidator.validateEmail(email)) {
                                emailInput.setError(getString(R.string.email_not_valid));
                                emailInput2.setError(getString(R.string.email_not_valid));
                            } else {
                                emailInput.setError(null);
                                emailInput2.setError(null);
                            }
                        }

                    } else {

                        for (TextInputLayout input : inputs) {
                            input.setError(null);
                        }

                        valid = true;
                    }
                } else {

                    if (donations.isEmpty()) {
                        Snackbar.make(firstNameInput, getString(R.string.need_add_donations), Snackbar.LENGTH_LONG).show();
                    } else {
                        valid = true;
                    }
                }

                if (valid) {


                    new AlertDialog.Builder(v.getContext(), R.style.AlertDialogCustom)
                            .setMessage(getString(R.string.are_you_sure_correct))
                            .setPositiveButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    showSignatureDialog(((AlertDialog) dialog).getContext());
                                }
                            })
                            .setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();

                }
            }

        });


        donationLayout.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (donationLayout.getChildCount() > 0) {
                    submitButton.setVisibility(View.VISIBLE);
                } else {
                    submitButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {
                if (donationLayout.getChildCount() > 0) {
                    submitButton.setVisibility(View.VISIBLE);
                } else {
                    submitButton.setVisibility(View.GONE);
                }
            }
        });

    }

    private void showSignatureDialog(final Context context) {


        final AlertDialog mydialog = new AlertDialog.Builder(context, R.style.AlertDialogCustom)
                .setTitle(getString(R.string.please_sign))
                .setView(R.layout.signature_dialog_layout)
                .setPositiveButton(getString(R.string.submit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        InkView inkView = (InkView) ((AlertDialog) dialog).findViewById(R.id.ink);
                        Bitmap signature = inkView.getBitmap();
                        dialog.dismiss();
                        submitForm(context, signature);

                    }
                })
                .setNegativeButton(getString(R.string.clear), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(true)
                .create();

        //Have to add Negative button onClick() in onShow() because otherwise it automatically closes

        mydialog.setOnShowListener(new DialogInterface.OnShowListener() {

            boolean alertReady = false;

            @Override
            public void onShow(DialogInterface dialog) {
                if (!alertReady) {
                    Button button = mydialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            InkView inkView = (InkView) mydialog.findViewById(R.id.ink);
                            inkView.clear();
                        }
                    });
                    alertReady = true;
                }
            }
        });
        mydialog.show();

    }

    private void submitForm(final Context context, final Bitmap signature) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String restoreZipCode = sharedPreferences.getString("zipCode", "");

        if (!restoreZipCode.isEmpty()) {

            final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AlertDialogCustom);
            progressDialog.setMessage(getString(R.string.submitting));
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

            progressDialog.getWindow().getDecorView().setMinimumWidth(Util.convertDpToPixel(400, context));

            AsyncTask<Void, Void, Void> connect = new AsyncTask<Void, Void, Void>() {

                String result = "";

                @Override
                protected Void doInBackground(Void... params) {


                    DonationForm donationForm = new DonationForm(firstName, lastName, address, city,
                            state, zip, homePhone, cellPhone, email, donations, restoreZipCode);
                    if (!newAccount) {
                        donationForm.setPersonID(personID);
                    }
                    try {
                        ByteArrayOutputStream out = new ByteArrayOutputStream(signature.getByteCount());

                        //Using WEBP because, just like PNG, it is lossless and has an alpha layer,
                        //but file sizes are reduced significantly. It also has anti-aliasing that
                        //makes signatures look less pixelated when zoomed in

                        signature.compress(Bitmap.CompressFormat.WEBP, 99, out);
                        out.flush();
                        donationForm.setSignatureByteArray(out.toByteArray());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    final String jsonData = new Gson().toJson(donationForm);

                    try {
                        final Socket client = new Socket(InetAddress.getByName(getString(R.string.server_ip)), 5000);
                        client.setSoTimeout(8000);
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(client.getOutputStream());
                        BufferedReader bf = new BufferedReader(new InputStreamReader(client.getInputStream()));
                        if (newAccount) {
                            outputStreamWriter.write(Intro.MODE_INSERT_NEW);
                        } else {
                            outputStreamWriter.write(Intro.MODE_INSERT_EXISTING);
                        }
                        writeQueue(outputStreamWriter);
                        outputStreamWriter.write(jsonData);
                        writeQueue(outputStreamWriter);
                        result = bf.readLine();
                        outputStreamWriter.close();
                        bf.close();
                        client.close();
                        Log.d("result", "result: " + result);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void df) {

                    progressDialog.dismiss();
                    if (result != null && !result.isEmpty()) {
                        if (result.equals(Intro.RESULT_SUCCESS)) {

                            AlertDialog dialog = new AlertDialog.Builder(addDonationBtn.getContext(), R.style.AlertDialogCustom)
                                    .setTitle(getString(R.string.thank_you))
                                    .setMessage(getString(R.string.thank_you_text))
                                    .setCancelable(true)
                                    .show();
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    finish();
                                }
                            });

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 5000);


                        } else if (result.equals(Intro.RESULT_FAILED))
                            Snackbar.make(donationLayout, getString(R.string.failure), Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(donationLayout, getString(R.string.error_occurred), Snackbar.LENGTH_LONG).show();
                    }


                }


            };
            connect.execute();

        } else {
            Snackbar.make(donationLayout, getString(R.string.zip_not_found), Snackbar.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            //New donation added

            if (resultCode == Activity.RESULT_OK) {
                final Donation donation = data.getParcelableExtra("result");
                donations.add(donation);
                final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View child = inflater.inflate(R.layout.donation_layout, null);
                final ImageView edit = (ImageView) child.findViewById(R.id.edit_button);
                ((TextView) child.findViewById(R.id.donation_item_name)).setText(donation.getName());
                ((TextView) child.findViewById(R.id.donation_item_quantity)).setText(String.valueOf(donation.getQuantity()));
                DecimalFormat df = new DecimalFormat("#");
                df.setMaximumFractionDigits(2);
                ((TextView) child.findViewById(R.id.donation_item_value)).setText("$" + df.format(donation.getValue()));
                String itemQuality;
                if (donation.is_new()) {
                    itemQuality = getString(R.string.condition_new);
                } else {
                    itemQuality = getString(R.string.used);
                }

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AddDonation.class);
                        intent.putExtra("donation", donation);
                        intent.putExtra("index", donationLayout.indexOfChild(child));
                        Log.d("TAG", String.valueOf(donationLayout.indexOfChild(child)));
                        startActivityForResult(intent, 2);
                        edit.setOnClickListener(null);
                    }
                });
                ((TextView) child.findViewById(R.id.donation_item_new)).setText(itemQuality);
                donationLayout.addView(child);

            }
        } else if (requestCode == 2) {

            //Donation edited
            final int index = data.getIntExtra("index", -1);
            final View view = donationLayout.getChildAt(index);
            final ImageView edit = (ImageView) view.findViewById(R.id.edit_button);

            if (resultCode == Activity.RESULT_OK) {

                donations.remove(index);
                if (data.getBooleanExtra("delete", false)) {
                    donationLayout.removeViewAt(index);
                } else {
                    final Donation donation = data.getParcelableExtra("result");
                    donations.add(index, donation);

                    Log.d("TAG", String.valueOf(index));
                    ((TextView) view.findViewById(R.id.donation_item_name)).setText(donation.getName());
                    ((TextView) view.findViewById(R.id.donation_item_quantity)).setText(String.valueOf(donation.getQuantity()));
                    DecimalFormat df = new DecimalFormat("#");
                    df.setMaximumFractionDigits(2);
                    ((TextView) view.findViewById(R.id.donation_item_value)).setText("$" + df.format(donation.getValue()));
                    String itemQuality;
                    if (donation.is_new()) {
                        itemQuality = getString(R.string.condition_new);
                    } else {
                        itemQuality = getString(R.string.used);
                    }
                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), AddDonation.class);
                            intent.putExtra("donation", donation);
                            intent.putExtra("index", donationLayout.indexOfChild(view));
                            Log.d("TAG", String.valueOf(donationLayout.indexOfChild(view)));
                            startActivityForResult(intent, 2);
                            edit.setOnClickListener(null);
                        }
                    });
                    ((TextView) view.findViewById(R.id.donation_item_new)).setText(itemQuality);

                }


            } else {
                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AddDonation.class);
                        intent.putExtra("donation", donations.get(donationLayout.indexOfChild(view)));
                        intent.putExtra("index", donationLayout.indexOfChild(view));
                        Log.d("TAG", String.valueOf(donationLayout.indexOfChild(view)));
                        startActivityForResult(intent, 2);
                        edit.setOnClickListener(null);
                    }
                });
            }
        }

        //delaying the scroll to give system time to add/remove views
        //so scrolling is less glitchy
        if (resultCode != RESULT_CANCELED) {
            final ScrollView s = ((ScrollView) findViewById(R.id.scrollView));
            s.postDelayed(new Runnable() {
                @Override
                public void run() {
                    s.fullScroll(View.FOCUS_DOWN);
                    donationLayout.getRootView().requestFocus();

                }
            }, 100);
        }

    }

    public static void writeQueue(OutputStreamWriter writer) {
        try {
            writer.write("\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        newAccount = inState.getBoolean("newAccount", true);
        if (newAccount) {

            try {
                firstNameInput.getEditText().setText(inState.getString("firstName"));
                lastNameInput.getEditText().setText(inState.getString("lastName"));
                addressInput.getEditText().setText(inState.getString("address"));
                zipInput.getEditText().setText(inState.getString("zip"));
                stateInput.getEditText().setText(inState.getString("state"));
                homePhoneInput.getEditText().setText(inState.getString("homePhone"));
                cellPhoneInput.getEditText().setText(inState.getString("cellPhone"));
                emailInput.getEditText().setText(inState.getString("email"));
                cityInput.getEditText().setText(inState.getString("city"));
            } catch (NullPointerException np) {
                //np for no problem :)
            }
        }
        donations = inState.getParcelableArrayList("donations");
        if (donations != null && donations.size() > 0) {

            final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (final Donation donation : donations) {

                final View child = inflater.inflate(R.layout.donation_layout, null);
                final ImageView edit = (ImageView) child.findViewById(R.id.edit_button);
                ((TextView) child.findViewById(R.id.donation_item_name)).setText(donation.getName());
                ((TextView) child.findViewById(R.id.donation_item_quantity)).setText(String.valueOf(donation.getQuantity()));
                DecimalFormat df = new DecimalFormat("#");
                df.setMaximumFractionDigits(2);
                ((TextView) child.findViewById(R.id.donation_item_value)).setText("$" + df.format(donation.getValue()));
                String itemQuality;
                if (donation.is_new()) {
                    itemQuality = getString(R.string.condition_new);
                } else {
                    itemQuality = getString(R.string.used);
                }

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AddDonation.class);
                        intent.putExtra("donation", donation);
                        intent.putExtra("index", donationLayout.indexOfChild(child));
                        Log.d("TAG", String.valueOf(donationLayout.indexOfChild(child)));
                        startActivityForResult(intent, 2);
                        edit.setOnClickListener(null);
                    }
                });
                ((TextView) child.findViewById(R.id.donation_item_new)).setText(itemQuality);
                donationLayout.addView(child);
            }

        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        if (newAccount) {
            try {
                outState.putString("firstName", firstNameInput.getEditText().getText().toString());
                outState.putString("lastName", lastNameInput.getEditText().getText().toString());
                outState.putString("address", addressInput.getEditText().getText().toString());
                outState.putString("zip", zipInput.getEditText().getText().toString());
                outState.putString("state", stateInput.getEditText().getText().toString());
                outState.putString("homePhone", homePhoneInput.getEditText().getText().toString());
                outState.putString("cellPhone", cellPhoneInput.getEditText().getText().toString());
                outState.putString("city", cityInput.getEditText().getText().toString());
                outState.putString("email", emailInput.getEditText().getText().toString());
                outState.putBoolean("newAccount", false);
            } catch (NullPointerException np) {
                //np for no problem :)
            }
        }
        outState.putParcelableArrayList("donations", donations);
        super.onSaveInstanceState(outState);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}


