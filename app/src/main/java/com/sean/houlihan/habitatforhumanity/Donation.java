package com.sean.houlihan.habitatforhumanity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sean on 12/21/2015.
 */
public class Donation implements Parcelable {

    private final String name;
    private final int quantity;
    private final double value;
    private final boolean is_new;


    public Donation(String name, int quantity, boolean is_new, double value) {
        this.name = name;
        this.quantity = quantity;
        this.is_new = is_new;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean is_new() {
        return is_new;
    }

    public double getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.quantity);
        dest.writeDouble(this.value);
        dest.writeByte(is_new ? (byte) 1 : (byte) 0);
    }

    protected Donation(Parcel in) {
        this.name = in.readString();
        this.quantity = in.readInt();
        this.value = in.readDouble();
        this.is_new = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Donation> CREATOR = new Parcelable.Creator<Donation>() {
        @Override
        public Donation createFromParcel(Parcel source) {
            return new Donation(source);
        }

        @Override
        public Donation[] newArray(int size) {
            return new Donation[size];
        }
    };
}

